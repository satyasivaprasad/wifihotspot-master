package com.fitc.wifihotspot

abstract class MyOnStartTetheringCallback {
    /**
     * Called when tethering has been successfully started.
     */
    abstract fun onTetheringStarted()

    /**
     * Called when starting tethering failed.
     */
    abstract fun onTetheringFailed()
}