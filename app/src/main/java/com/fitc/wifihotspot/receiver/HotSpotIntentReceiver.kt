package com.fitc.wifihotspot.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.fitc.wifihotspot.MagicActivity
import com.fitc.wifihotspot.R

class HotSpotIntentReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val ACTION_TURNON = context.getString(R.string.intent_action_turnon)
        val ACTION_TURNOFF = context.getString(R.string.intent_action_turnoff)
        Log.i(TAG, "Received intent")
        if (intent != null) {
            val action = intent.action
            if (ACTION_TURNON == action) {
                MagicActivity.useMagicActivityToTurnOn(context)
            } else if (ACTION_TURNOFF == action) {
                MagicActivity.useMagicActivityToTurnOff(context)
            }
        }
    }

    companion object {
        private val TAG = HotSpotIntentReceiver::class.java.simpleName
    }
}