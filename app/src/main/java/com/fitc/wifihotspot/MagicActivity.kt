package com.fitc.wifihotspot

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import com.fitc.wifihotspot.HotSpotIntentService.Companion.start
import java.util.concurrent.TimeUnit

class MagicActivity : PermissionsActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e(TAG, "onCreate")
    }

    public override fun onPermissionsOkay() {
        carryOnWithHotSpotting()
    }

    /**
     * The whole purpose of this activity - to start [HotSpotIntentService]
     * This may be called straright away in `onCreate` or after permissions granted.
     */
    private fun carryOnWithHotSpotting() {
        val intent = intent
        start(this, intent)
        finish()
    }

    companion object {
        @JvmStatic
        fun useMagicActivityToTurnOn(c: Context) {
            val uri = Uri.Builder().
            scheme(c.getString(R.string.intent_data_scheme))
                    .authority(c.getString(R.string.intent_data_host_turnon))
                    .build()
            Toast.makeText(c, "Turn on. Uri: $uri", Toast.LENGTH_LONG).show()
            val i = Intent(Intent.ACTION_VIEW)
            i.data = uri
            i.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            c.startActivity(i)
        }

        @JvmStatic
        fun useMagicActivityToTurnOff(c: Context) {
            val uri = Uri.Builder()
                    .scheme(c.getString(R.string.intent_data_scheme))
                    .authority(c.getString(R.string.intent_data_host_turnoff))
                    .build()
            Toast.makeText(c, "Turn off. Uri: $uri", Toast.LENGTH_LONG).show()
            val i = Intent(Intent.ACTION_VIEW)
            i.data = uri
            i.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            c.startActivity(i)
        }

        private val TAG = MagicActivity::class.java.simpleName
    }
}