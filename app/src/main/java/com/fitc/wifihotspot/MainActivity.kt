package com.fitc.wifihotspot

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.TextView
import com.fitc.wifihotspot.MagicActivity.Companion.useMagicActivityToTurnOff
import com.fitc.wifihotspot.MagicActivity.Companion.useMagicActivityToTurnOn

class MainActivity : PermissionsActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val instructTv = findViewById<TextView>(R.id.instructions_tv)
        instructTv.movementMethod = LinkMovementMethod.getInstance()
        val actionsNoteTv = findViewById<TextView>(R.id.actions_note_tv)
        actionsNoteTv.movementMethod = LinkMovementMethod.getInstance()
        val linkonTv = findViewById<TextView>(R.id.linkon_tv)
        linkonTv.movementMethod = LinkMovementMethod.getInstance()
        val linkoffTv = findViewById<TextView>(R.id.linkoff_tv)
        linkoffTv.movementMethod = LinkMovementMethod.getInstance()


    }

    var isTurnedOn = false

    public override fun onPermissionsOkay() {}
    fun onClickTurnOnAction(v: View?) {
        val intent = Intent(getString(R.string.intent_action_turnon))
        sendImplicitBroadcast(this, intent)
    }

    fun onClickTurnOffAction(v: View?) {
        val intent = Intent(getString(R.string.intent_action_turnoff))
        sendImplicitBroadcast(this, intent)
    }

    fun onClickTurnOnData(v: View?) {
        useMagicActivityToTurnOn(this)
    }

    fun onClickTurnOffData(v: View?) {
        useMagicActivityToTurnOff(this)
    }



    private val autoHotspotStatusHandler = Handler(Looper.getMainLooper())

    private val autoHotspotStatusRunnable: Runnable = Runnable {
        if (isTurnedOn) {
            isTurnedOn = false
            useMagicActivityToTurnOff(this)
        } else {
            isTurnedOn = true
            useMagicActivityToTurnOn(this)
        }
        startHotspotStatusMonitor()
    }

    private fun startHotspotStatusMonitor() {
        autoHotspotStatusHandler.postDelayed(autoHotspotStatusRunnable, 20000)
    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName
        private const val SHOW_ICON = "show_icon"
        private fun sendImplicitBroadcast(ctxt: Context, i: Intent) {
            val pm = ctxt.packageManager
            val matches = pm.queryBroadcastReceivers(i, 0)
            for (resolveInfo in matches) {
                val explicit = Intent(i)
                val cn = ComponentName(resolveInfo.activityInfo.applicationInfo.packageName,
                        resolveInfo.activityInfo.name)
                explicit.component = cn
                ctxt.sendBroadcast(explicit)
            }
        }
    }

    fun onClickAutoAction(view: View) {
        startHotspotStatusMonitor()
    }
}